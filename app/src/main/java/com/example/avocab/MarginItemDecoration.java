package com.example.avocab;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Décoration destiné aux items d'un recyclerView.
 */
public class MarginItemDecoration extends RecyclerView.ItemDecoration {

    // la hauteur des marges latérales
    private final int spaceSize;

    public MarginItemDecoration (int spaceSize) {
        this.spaceSize = spaceSize;
    }

    @Override
    /**
     * Application des marges sur la forme de l'item, à travers le rectangle outRect de l'item view
     * @param outRect La forme de l'item qui va être appliquée
     * @param view L'item à modifier
     */
    public void getItemOffsets (Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = this.spaceSize;
        }
        outRect.left = this.spaceSize;
        outRect.right = this.spaceSize;
        outRect.bottom = this.spaceSize;
    }
}
