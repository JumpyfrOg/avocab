package com.example.avocab;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


/**
 * The activity of the settings page. Display pref_layout and the settings fragment.
 **/
public class PrefActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pref_layout);

        Toolbar mTopToolbar = findViewById(R.id.pref_toolbar);
        setSupportActionBar(mTopToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // overide "back" button of the phone to recall the main activity and apply modification
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // create a new intent to prepare switching activity
                Intent intent = new Intent(PrefActivity.this, ListActivity.class);

                // put a flag to specify this activity is closed and go back to the previous which
                // is the main activity of the app
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // start activity
                startActivity(intent);
            }
        };
        getOnBackPressedDispatcher().addCallback(this, callback);


        if ( findViewById(R.id.fragment_container) != null ) {
            if ( savedInstanceState != null ) {
                return;
            }
            getFragmentManager().beginTransaction().add(R.id.fragment_container,
                    new PrefFragment()).commit();   // Display the settings fragment
        }

    }

    @Override
    /**
     * Executée lorsque que le bouton de retour de la toolbar est appuyé.
     */
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
