package com.example.avocab;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ManipString {

    public static List<Model> sortFr (List<Model> list) {
        List<Model> ret = new ArrayList<>();

        // parcours pour chaque mot de la liste
        for ( int i = 0; i < list.size(); i++ ) {
            int index_minus = 0;
            for ( int j = 0; j < list.size(); j++ ) {
                if ( !ret.contains(list.get(j)) ) {
                    index_minus = j;
                    break;
                }
            }

            // parcours pour trouver le mot le plus proche de A dans la liste
            for ( int j = 1; j < list.size(); j++ ) {
                if ( list.get(j).getFr().replace("é","e")
                        // supprimer les accents pour les deux mots de la comparaison
                        .replace("è","e")
                        .replace("ê","e")
                        .replace("ô","o")
                        .replace("à","a").compareTo(list.get(index_minus).getFr()
                            .replace("é","e")
                            .replace("è","e")
                            .replace("ê","e")
                            .replace("ô","o")
                            .replace("à","a")) < 0 && // j est plus proche de A que index_minus
                    !ret.contains(list.get(j)) ) {
                    index_minus = j;
                }
            }

            // ajout du string le plus bas (proche de A)
            ret.add(list.get(index_minus));
        }
        return ret;
    }

    public static List<Model> sortAng (List<Model> list) {
        List<Model> ret = new ArrayList<>();

        // parcours pour chaque mot de la liste
        for ( int i = 0; i < list.size(); i++ ) {
            int index_minus = 0;
            for ( int j = 0; j < list.size(); j++ ) {
                if ( !ret.contains(list.get(j)) ) {
                    index_minus = j;
                    break;
                }
            }

            // parcours pour trouver le mot le plus proche de A dans la liste
            for ( int j = 1; j < list.size(); j++ ) {
                if ( list.get(j).getAng().toUpperCase().compareTo(list.get(index_minus).getAng().toUpperCase()) < 0 && // j est plus proche de A que index_minus
                        !ret.contains(list.get(j)) ) {
                    index_minus = j;
                }
            }

            // ajout du string le plus bas (proche de A)
            ret.add(list.get(index_minus));
        }
        return ret;
    }
}
