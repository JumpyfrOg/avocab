package com.example.avocab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * Fragment of preference list.
 **/

public class PrefFragment extends PreferenceFragment{

    /**
     * Redefinition of depreciated default constructor
     */
    public PrefFragment(){super();}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_xml);

        // disabled boot preference if notiffications are not allow
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if ( !sharedPref.getBoolean(getResources().getString(R.string.settings_sort_enable_key),false) ) {
            Preference notif_boot = findPreference(getResources().getString(R.string.settings_type_sort_key));
            notif_boot.setEnabled(false);
        }


        // disabled or enabled boot preference when notifications preferences are pressed
        Preference sort_enable = (Preference) findPreference(getResources().getString(R.string.settings_sort_enable_key));
        sort_enable.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Preference sort_choice = findPreference(getResources().getString(R.string.settings_type_sort_key));
                sort_choice.setEnabled((boolean)o);
                return true;
            }
        });
    }


    /**
     * FOR DEBUG
     * Print in log all keys saved from the preferences screen.
     */
//    public static void mapSharePreference(Context context){
//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
//        Map<String, ?> allEntries = sharedPref.getAll();
//        Log.i(" - - - - - - - Shares preference values ",":");
//        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
//            Log.i(" - - - - - - - map values", entry.getKey() + ": " + entry.getValue().toString()+" - - - - - - - ");
//        }
//    }

}
