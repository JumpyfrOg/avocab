package com.example.avocab;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/**
 * Classe qui se charge des animations des fav boutons à travers ses différentes fonctions.
 */
public class ViewAnimation {

    /**
     * Pivote la vue passée en paramètre selon si elle est dans l'état de pivoter
     * (paramètre rotate). Si true est passé en paramètre, la vue pivote de 180° vers la gauche.
     * Tandis que si false est passé en paramètre, rien ne bouge. Elle retourne l'état de pivotation
     * passé en paramètre. La rotation dure 0.3 secondes.
     * @param v La vue à pivoter
     * @param rotate Si oui ou non la vue doit pivoter
     * @return La valeur du paramètre rotate
     */
    public static boolean rotateFab(final View v, boolean rotate) {
        v.animate().setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .rotation(rotate ? 180f : 0f);
        return rotate;
    }


    /**
     * Affiche la vue passée en paramètre avec une transition. Elle s'affiche (car initialement
     * invisible) progressivement et se déplace légèrement de bas en haut. La transition dure 0.2
     * secondes.
     * @param v La vue à animer
     */
    public static void showIn(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(0f);
        v.setTranslationY(v.getHeight());
        v.animate()
                .setDuration(200)
                .translationY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(1f)
                .start();
    }

    /**
     * Animation inverse de showIn.
     * @param v La vue à animer
     */
    public static void showOut(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(1f);
        v.setTranslationY(0);
        v.animate()
                .setDuration(200)
                .translationY(v.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setVisibility(View.GONE);
                        super.onAnimationEnd(animation);
                    }
                }).alpha(0f)
                .start();
    }

    /**
     * Initialise la vue passée en paramètres pour l'utiliser dans des transitions.
     * @param v La vue à initialiser
     */
    public static void init(final View v) {
        v.setVisibility(View.GONE);
        v.setTranslationY(v.getHeight());
        v.setAlpha(0f);
    }


}
