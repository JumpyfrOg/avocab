package com.example.avocab;

import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

	// la liste de mot
    private List<Model> list;

    // la liste de mot trié selon le critère sélectionné dans les paramètre
    private List<Model> sortedList;
    // booleen qui témoigne si le paramètre tri est activé et donc que la liste d emot doit l'être
    private boolean is_sorted;

    // témoin de rotation du fav bouton
    boolean isRotate = false;

    // variables nécéssaires pour l'exportation/importation
    private final static int REQUEST_CODE = 2021;
    private static final String LOG_TAG = "ExternalStorage_";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);
        // ajout de la toolbar du layout pour ajouter le bouton de retour et le bouton "delete"
        Toolbar mTopToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(mTopToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

		// charger les données sauvegardées
        this.list = RWFile.load(this);
        // si la liste est vide, afficher le label qui informe que la liste est vide
        if ( this.list.size() == 0 ) {
            findViewById(R.id.warning_list).setVisibility(View.VISIBLE);
        }

        // récupération de la valeur du paramètre de tri
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        this.is_sorted = sharedPref.getBoolean(getResources().getString(R.string.settings_sort_enable_key),false);
        // si oui, et selon le critère de tri (ang ou fr) trier la liste
        if ( this.is_sorted ) {
            // tri par défaut en fr
            String type_sort = sharedPref.getString(getResources().getString(R.string.settings_type_sort_key),"fr");
            if ( type_sort.equals("fr") ) {
                this.sortedList = ManipString.sortFr(this.list);
            }else if ( type_sort.equals("ang") ) {
                this.sortedList = ManipString.sortAng(this.list);
            }else{
                this.is_sorted = false;
            }
        }
        this.updateList();




        /* INITIALISATION DES BUTTONS */

        // initialisation des mini fav boutons
        ViewAnimation.init(findViewById(R.id.main_fab));
        ViewAnimation.init(findViewById(R.id.fab_upload));
        ViewAnimation.init(findViewById(R.id.fab_export));

		// listener pour le bouton fav principal
        FloatingActionButton but = findViewById(R.id.button_add);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    isRotate = ViewAnimation.rotateFab(view,!isRotate);
                    if(isRotate){
                        ViewAnimation.showIn(findViewById(R.id.main_fab));
                        ViewAnimation.showIn(findViewById(R.id.fab_upload));
                        ViewAnimation.showIn(findViewById(R.id.fab_export));
                    }else{
                        ViewAnimation.showOut(findViewById(R.id.main_fab));
                        ViewAnimation.showOut(findViewById(R.id.fab_upload));
                        ViewAnimation.showOut(findViewById(R.id.fab_export));
                    }
            }
        });

        // listener du bouton d'ajout
        findViewById(R.id.main_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // créer un nouvel intent pour changer d'activité
                Intent intent = new Intent(ListActivity.this,ModifActivity.class);

				// informer que c'est un ajout en passant -1 comme position
                intent.putExtra("position",-1);

                // ajouter un drapeau pour spécifier que la nouvelle activité se place sur celle
                // actuelle
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // lancement de la nouvelle activité
                startActivity(intent);
            }
        });

        // listener du bouton d'upload de fichier
        findViewById(R.id.fab_upload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int FILE_SELECT_CODE = 2021;

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

                // Update with mime types
                intent.setType("*/*");

                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(
                            // ouvrir un sélecteur de fichier
                            Intent.createChooser(intent, "Choisissez un fichier JSON à importer"),
                            FILE_SELECT_CODE);
                    Toast.makeText(ListActivity.this,"Seul les fichiers JSON sont compatibles à l'importation", Toast.LENGTH_LONG).show();
                }catch (Exception e) {}
            }


        });

        // listener du bouton d'export
        findViewById(R.id.fab_export).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ContextWrapper cw = new ContextWrapper(getApplicationContext());
                new AlertDialog.Builder(ListActivity.this)
                        .setTitle("Attention")
                        .setMessage("Voulez vous vraiment exporter la liste de mots de vocabulaire à cet endroit : \n" + cw.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS) +"/avocab.json ?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    ActivityCompat.requestPermissions(ListActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},23);
                                    String data = "{\n";
                                    for ( int i = 0; i < list.size(); i++ ) {
                                        data += "\t\"" + list.get(i).getAng() + "\": \"" + list.get(i).getFr() +"\"";
                                        if ( i + 1 != list.size() ) {
                                            data += ",";
                                        }
                                        data += "\n";
                                    }
                                    data += "}";

                                    File directory = cw.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                                    File file = new File(directory, "avocab" + ".json");

                                    FileOutputStream fOut = new FileOutputStream(file);
                                    fOut.write(data.getBytes("UTF-8"));
                                    fOut.flush();
                                    fOut.close();

                                    StringBuffer sbuffer = new StringBuffer();
                                    String details[] = sbuffer.toString().split("\n");

                                    Toast.makeText(getApplicationContext(), "Exportation réussie", Toast.LENGTH_LONG).show();
                                }catch (Exception e){
                                    Toast.makeText(getApplicationContext(), "Echec de l'exportation", Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        // listener du bouton de suppression
        ImageButton toolBarBtn = (ImageButton) findViewById(R.id.btn_photo_lib);
        toolBarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nb = ((RecyclerViewAdapter) ((RecyclerView) findViewById(R.id.listview)).getAdapter()).getSelectedModels().size();
                if ( nb != 0 ) {
                    String txt = "";
                    if ( nb == 1 ) {
                        txt = "Voulez vous vraiment supprimer cet élément ?";
                    }else{
                        txt = "Voulez vous vraiment supprimer ces " + nb + " éléments ?";
                    }
                    new AlertDialog.Builder(ListActivity.this)
                            .setTitle("Attention")
                            .setMessage(txt)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    deletePressed();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }
            }
        });

        // listener du bouton de paramètre
        ImageButton toolBarBtnSet = (ImageButton) findViewById(R.id.btn_settings);
        toolBarBtnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // créer un nouvel intent pour changer d'activité
                Intent intent = new Intent(ListActivity.this, PrefActivity.class);

                // ajouter un drapeau pour spécifier que la nouvelle activité se place sur celle
                // actuelle
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // lancement de la nouvelle activité
                startActivity(intent);
            }
        });
    }



    @Override
    /**
     * Executée lorsque que le bouton de retour de la toolbar est appuyé.
     */
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    /**
     * Appelée lorque l'utilisateur a choisi un fichier dans le sélecteur.
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String filename = data.getData().toString().toUpperCase();
        // si l'user à choisi un fichier JSON
        if ( (filename.split("\\.")[filename.split("\\.").length-1]).equals("JSON") ||
                (filename.split("\\.")[filename.split("\\.").length-1]).equals("json") ){
            try {
                // Ouvrir un stream de lecture sur l'URI du fichier sélectioné
                InputStream in = getContentResolver().openInputStream(data.getData());

                // lire et stocker les lignes du fichier
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                StringBuilder total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }
                String content = total.toString();

                // Convertir le string en objet JSON
                JSONObject obj = new JSONObject(content);

                // Ajouter les nouveaux mots à la liste actuelle (ajout à la fin)
                JSONArray jArray = obj.names();
                for (int i = 0; i < jArray.length(); i++) {
                    // nom = clé du couple ( -> mot ang )
                    // getString = valeur de la clé ( -> mot fr )
                    String nom = (String) jArray.get(i);
                    this.list.add(new Model(nom,obj.getString(nom)));
                }

                // mettre à jour l'affichage
                this.updateList();
                RWFile.save(this,this.list);

                // informer le succès de l'opération
                Toast.makeText(this, "Les mots ont été ajoutés en fin de liste.", Toast.LENGTH_LONG).show();


            } catch (Exception e) {Toast.makeText(this,"L'importation a échouée",Toast.LENGTH_LONG).show();e.printStackTrace();}
        }else{
            Toast.makeText(this,"L'importation a échouée",Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Mettre à jour l'affichage de la liste sur l'application. Les mots affichés sont ceux de la
     * liste "list".
     */
    public void updateList() {
        // ajoute le layout des mots de la liste
        RecyclerView view = findViewById(R.id.listview);
        RecyclerView.Adapter mAdapter;
        // si le paramètre de tri est activé, donner la liste trié au recycler
        if ( this.is_sorted ) {
            mAdapter = new RecyclerViewAdapter(this.sortedList, this);
        }else {
            mAdapter = new RecyclerViewAdapter(this.list, this);
        }
        LinearLayoutManager manager = new LinearLayoutManager(ListActivity.this);
        ((RecyclerView)view).setHasFixedSize(true);
        view.setLayoutManager(manager);
        view.setAdapter(mAdapter);

        // si les marges n'ont pas déjà été appliquées aux couples
        if ( view.getItemDecorationCount() == 0 ) {
            view.addItemDecoration(new MarginItemDecoration(50));
        }

        // masquer le label d'avertissement si la liste n'est pas vide
        findViewById(R.id.warning_list).setVisibility(this.list.size() == 0 ? View.VISIBLE:View.INVISIBLE);
    }








    /**
     * Lance l'activité "ModifActivity" après qu'une case de l'application ait été touchée, renvoyant sa position
     * dans la liste "list" (non trié) en paramètre de la méthode. Cette même position est envoyée à travers le changement d'activité
     * sous le nom "position".
     * @param position La position du couple dans la liste non trié sauvegardé dans l'application
     */
    public void itemOnClick (int position) {
        // créer un nouvel intent pour changer d'activité
        Intent intent = new Intent(ListActivity.this,ModifActivity.class);

        // passer la position du couple à modifier
        intent.putExtra("position",position);
        // TODO changer le string "position" par un str global/général

        // ajouter un drapeau pour spécifier que la nouvelle activité se place sur celle
        // actuelle
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // lancement de la nouvelle activité
        startActivity(intent);
    }


    public Toolbar getToolbar () {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        return toolbar;
    }


    /**
     * Lorsque le bouton "supprimer" (icone poubelle de la toolbar) est actionnée, tous les couples
     * sélectionné (présent dans la liste "selectedModels") sont supprimer de "list"
     */
    public void deletePressed () {
        List<Model> selectedModels = ((RecyclerViewAdapter) ((RecyclerView) findViewById(R.id.listview)).getAdapter()).getSelectedModels();
        for ( int i = 0; i < selectedModels.size() ; i++ ) {
            this.list.remove((selectedModels.get(i)));
        }
        // mettre à jour la liste affichée, quitter le mode de sélection, sauvegarder la nouvelle liste
        this.updateList();
        ((RecyclerViewAdapter) ((RecyclerView) findViewById(R.id.listview)).getAdapter()).initial_state(this.list);
        RWFile.save(this,this.list);
    }


    /**
     * Exécutée lorsque le bouton retour de la toolbar ou le bouton retour de l'appareil sont actionnés.
     * L'activité principale "MainActivity" est lancé.
     */
    public void onBackPressed() {
        // créer un nouvel intent pour changer d'activité
        Intent intent = new Intent(ListActivity.this,MainActivity.class);

        // ajouter un drapeau pour spécifier que la nouvelle activité est la principale
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // lancement de la nouvelle activité
        startActivity(intent);
    }


    /**
     * Utilisé lorsque le tri des mots est activé. Elle permet d'obtenir l'indice d'un couple dans
     * la liste sauvegardé dans l'application à partir de son indice dans une liste trié.
     * @param sortedIndex
     * @return
     */
    public int getNotSortedIndex(int sortedIndex) {
        // TODO : doc des paramètres
        int ret = -1;
        if ( !this.is_sorted ) {
            ret = sortedIndex;
        }else{
            int i = 0;
            // trouver dans la liste non trié, le couple égal à celui cherché, puis garder son indice
            while ( i < this.list.size() ) {
                if ( this.list.get(i).equals(this.sortedList.get(sortedIndex)) ) {
                    ret = i;
                    break;
                }
                i++;
            }
        }
        return ret;
    }


}
