package com.example.avocab;

/**
 * Classe représentant un couple de mot français et anglais représenté dans le recyclerView de ListActivity.
 * Elle contient les mots français et anglais ainsi qu'un booléen informant l'état sélectionné ou non
 * dans le recyclerView.
 */
public class Model{

    // le mot anglais du couple
    private String ang;

    // le mot français du couple
    private String fr;

    // booléen qui témoigne si le couple à été sélectionné ou non
    private boolean isSelected = false;

    /**
     * Constructeur qui requiert les mots du couple
     * @param textAng Le mot anglais du couple
     * @param textFr Le mot français du couple
     */
    public Model(String textAng, String textFr) {
        this.ang = textAng;
        this.fr = textFr;
    }

    @Override
    /**
     * Rend deux objets égaux si le mot français et anglais des couples sont les mêmes.
     * @param o
     * @return
     */
    public boolean equals(Object o) {
        Model m = (Model) o;
        boolean ret = false;
        if ( m.getFr() == this.getFr() && this.getAng() == m.getAng() ) {
            ret = true;
        }
        return ret;
    }

    // Getteurs
    public String getAng() {
        return this.ang;
    }
    public String getFr() {
        return this.fr;
    }
    public boolean isSelected() {
        return isSelected;
    }

    // Setteur
    public void setAng(String a) {
        this.ang = a;
    }
    public void setFr(String f){
        this.fr = f;
    }
    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String toString() {return "("+this.ang+","+this.fr+")";}
}
