package com.example.avocab;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Composant qui affiche une liste d'item comme une liste à la même manière qu'une ListView, au point
 * près qu'il est possible de personaliser les items (ajout de marges) et de pouvoir paramètrer le clic
 * sur un item (reconnaissance d'un clic simple et d'un clic long pour la sélection).
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    // La liste de couple de mots de l'application
    private List<Model> mModelList;
    // La liste de couple de mots sélectionnés par l'user
    public List<Model> selectedModel;

    // L'activité en cours qui affiche ce recyclerView
    private ListActivity listeActivity;

    // Variables utilisées pour la reconnaissance d'un clic long
    // timer qui calcule la durée d'un clic
    private Timer timer;
    // booléen qui témoigne qu'un timer est programmé (que l'utilisateur essaie d'effectuer un clic long)
    private boolean timer_scheduled;
    // booléen qui témoigne que le dernier clic de l'utilisateur est bien un clic long
    private boolean isTouch = false;

    // booléen qui témoigne que le mode "sélection" est activé ou non. En mode sélection, un clic simple
    // permet d'ajouter le couple à la liste de sélection.
    private boolean selectedMode = false;


    /**
     * Constructeur qui requiert la liste de couple de mot de l'application ainsi que l'activité qui
     * l'affiche.
     * @param modelList La liste de mot de l'application
     * @param list L'activité qui l'utilise et l'affiche
     */
    public RecyclerViewAdapter(List<Model> modelList, ListActivity list) {
        this.listeActivity = list;
        this.initial_state(modelList);
    }

    @Override
    /**
     * Exécutée automatiquement pour créer le recyclerViewHolder
     */
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // passage en paramètre du layout de l'item du recyclerView
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_string, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Model model = mModelList.get(position);
        holder.setText(mModelList.get(position).getAng(),mModelList.get(position).getFr());

        holder.view.setBackgroundColor(model.isSelected() ?
                listeActivity.getResources().getColor(R.color.colorSelectionToolbar) : Color.WHITE);
        holder.view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(final View view, MotionEvent m) {

                // Exécuté quand c'est une longue pression
                class TouchTimerTask extends TimerTask {
                    public TouchTimerTask() {}
                    public void run() {
                        isTouch = true;
                        selectedMode = true;
                        itemSelected(model, holder);
                        System.out.println("Long"+timer_scheduled);
                    }
                }


                // lorsque l'user pose le doigt sur l'écran
                if ( m.getAction() == MotionEvent.ACTION_DOWN ) {
                    System.out.println("\n");
                    System.out.println("in");
                    // lancer le timer pour percevoir un clic long ou court
                    timer = new Timer();
                    TouchTimerTask timer_task = new TouchTimerTask();
                    timer.schedule(timer_task, 1000); // programmer l'execution pour danss 0.5 sec
                    timer_scheduled = true; // informe qu'une tache est prévue
                }

                // lorsque l'user retire le doigt de l'écran
                if ( m.getAction() == MotionEvent.ACTION_UP || m.getAction() == MotionEvent.ACTION_CANCEL ) {
                    System.out.println("out");
                    // arrêter le timer
                    if ( timer != null && timer_scheduled ) {
                        timer.cancel();
                        timer = null;
                        // informe qu'aucune tache n'est prévu
                        timer_scheduled = false;
                        //isTouch = false;    // informer que le clic est simple
                    }
                }


                // gérer les réactions selon le mode de sélection (active ou non)
                if ( (m.getAction() == MotionEvent.ACTION_DOWN || m.getAction() == MotionEvent.ACTION_UP) &&
                        !timer_scheduled ) {    // la nature du clic est déterminé et non en attente
                    System.out.println("enter status : isTouch-"+isTouch+" _ selectedMode-"+selectedMode);

                    if (!selectedMode) {  // sélection désativée
                        if (!isTouch) {   // clic court
                            // itemOnClick demande l'indice non trié pour ModifActivity qui ne refait
                            // pas le tri de la liste
                            listeActivity.itemOnClick(listeActivity.getNotSortedIndex(position));
                            System.out.println("court");
                        } else {
                            itemSelected(model, holder);
                        }
                    } else {
                        if (!isTouch) {   // clic court
                            itemSelected(model, holder);
                        } else {
                            // rien
                        }
                    }
                    isTouch = false;    // status par défaut
                }


                System.out.println("End state : mode-"+selectedMode+"\n\n");

                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModelList == null ? 0 : mModelList.size();
    }

    /**
     * Classe personnel de ViewHolder, classe gérant l'affichage de RecyclerView. Elle s'occupe de
     * placer les mots des couples dans les items ainsi que changé la couleur selon l'état sélectionné
     * ou non de l'item.
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        // la view (RecyclerView) à gérer
        private View view;

        // TODO : simplification du code
//        private TextView textView;
//        private String ang;
//        private String fr;

        private MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
//            textView = (TextView) itemView.findViewById(R.id.fr_label);
        }

        private void setText (String ang, String fr) {
            // récupération des label et ajout des mots du couple
            TextView v = view.findViewById(R.id.fr_label);
            v.setText(fr);
            v = view.findViewById(R.id.tiret);
            v.setText("-");
            v = view.findViewById(R.id.ang_label);
            v.setText(ang);
        }
    }

    public void itemSelected(Model model, MyViewHolder holder) {
        model.setSelected(!model.isSelected());
        holder.view.setBackgroundColor(model.isSelected() ?
                listeActivity.getResources().getColor(R.color.colorSelectionToolbar) : Color.WHITE);


        if ( model.isSelected() ) {
            this.selectedModel.add(model);
        }else{
            this.selectedModel.remove(model);
        }
        if ( this.selectedMode && this.selectedModel.size() == 0 ) {
            this.initial_state(this.mModelList);
        }else{
            (listeActivity).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toolbar toolbar = listeActivity.getToolbar();
                    toolbar.setTitle("\t"+selectedModel.size()+" sélectioné(s)");
                    toolbar.setBackgroundColor(listeActivity.getResources().getColor(R.color.colorSelectionToolbarDarker));

                    ImageButton toolBarBtn = (ImageButton) listeActivity.findViewById(R.id.btn_photo_lib);
                    toolBarBtn.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public void initial_state( List<Model> list ) {
        this.mModelList = list;
        this.selectedMode = false;
        this.selectedModel = new ArrayList<>();
        this.timer = null;
        this.timer_scheduled = false;
        this.isTouch = false;
        (listeActivity).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toolbar toolbar = listeActivity.getToolbar();
                toolbar.setTitle(R.string.app_name);
                toolbar.setBackgroundColor(listeActivity.getResources().getColor(R.color.colorPrimary));

                ImageButton toolBarBtn = (ImageButton) listeActivity.findViewById(R.id.btn_photo_lib);
                toolBarBtn.setVisibility(View.INVISIBLE);
            }
        });
    }

    public List<Model> getSelectedModels () {return this.selectedModel;}
}
