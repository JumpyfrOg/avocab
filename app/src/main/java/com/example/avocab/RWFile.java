package com.example.avocab;

import android.app.Activity;
import android.widget.Toast;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RWFile {

    /**
     * Méthode pour charger les mots sauvegardés
     **/
    public static List<Model> load (Activity main) {
        // TODO : Changer le format tableau par liste
        List<Model> ret = new ArrayList<>();

        // sortir les mots et les mettres dans une arraylist
        ArrayList<String[]> str = new ArrayList<>();

        try {
            //Get the file where data are saved
            File directory = main.getFilesDir();
            File file = new File(directory, "data_file.txt");

            boolean cont = true;

            //Create stream to pass data from the file through the application
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fis);

            //if the reader isn't at the end of the file
            while (cont) {
                //get the next object wrote in the file
                //envoie une EOFException puisque qu'arriver à la fin de lecture il crash
                Object obj = is.readObject();
                if (obj != null) {
                    str.add((String[]) obj);
                }else{
                    cont = false;
                }
            }


            //close  stream
            is.close();
            fis.close();

            //If the file doesn't exist (exception made just at the begining, at the first launch of the app)
        } catch (Exception e) {
            if (e.getClass() == EOFException.class ) {

                // création du tableau, attribut de la classe, selon le nombre de mot extrait
                for (int i = 0; i < str.size(); i++) {
                    ret.add(new Model(str.get(i)[0], str.get(i)[1]));
                }
            }
        }
        return ret;
    }



    /**
     * Méthode pour sauvegarder les couples de mot
     **/
    public static boolean save (Activity main, List<Model> list) {
        // TODO : Changer le format tableau par liste
        boolean ret = false;
        try {

            //get the file to store tasks, or create it
            File directory = main.getFilesDir();
            File file = new File(directory, "data_file.txt");

            //create stream to pass data between the file and the app
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            //for each couple in the list, save one by one in the file
            for ( int i = 0; i < list.size(); i++ ) {
                String[] row = new String[2];
                row[0] = list.get(i).getAng();
                row[1] = list.get(i).getFr();
                oos.writeObject(row);
            }

            //close stream
            oos.close();
            fos.close();

            // La sauvegarde est un succès
            ret = true;


            //if a exception is throw during the saving, inform the user that the backup didn't work
        }catch(Exception e){
            Toast.makeText(main,"Echec de la sauvegarde",Toast.LENGTH_LONG).show();
        }
        return ret;
    }

}
