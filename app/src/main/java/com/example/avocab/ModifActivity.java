package com.example.avocab;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Activité qui se charge de modifier, ou ajouter, un couple de mot.
 */
public class ModifActivity extends AppCompatActivity {

	// la liste de mot
    private List<Model> list;

	// la position du couple à modifier dans la liste. -1 si c'est un ajout
    private int position;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modif_layout);
        // ajout de la toolbar du layout pour ajouter le bouton de retour, le bouton "delete" et "ok"
        Toolbar mTopToolbar = findViewById(R.id.modif_toolbar);
        setSupportActionBar(mTopToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle b = this.getIntent().getExtras();
        // chargement des données
        this.list = RWFile.load(this);

        // récupération de l'indice du couple sélectionné dans l'ancienne activité
        this.position = b.getInt("position");

		// si ce n'est pas un ajout, mettre les mots du couple dans les inputs pour pouvoir les modifier
        if (this.position != -1) {
            EditText txt = findViewById(R.id.input_fr);
            txt.setText(this.list.get(this.position).getFr());
            txt = findViewById(R.id.input_ang);
            txt.setText(this.list.get(this.position).getAng());
            
        // sinon changer le texte des boutons
        } else {
            ImageButton but = findViewById(R.id.btn_delete);
            but.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
            but.setEnabled(false);
        }

		// listener bouton "ok"
        ImageButton but = findViewById(R.id.btn_save);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean then = true;
				// si c'est un ajout
                if (position == -1) {
                    EditText txtFr = findViewById(R.id.input_fr);
                    EditText txtAng = findViewById(R.id.input_ang);
                    if ( !txtFr.getText().toString().equals("") && !txtAng.getText().toString().equals("") ) {
                        // créer un tableau avec une place en plus, copier le tableau et ajouter le nouveau couple
//                        String[][] newArray = new String[list.size + 1][2];
//                        for (int i = 0; i < list.length; i++) {
//                            newArray[i][0] = list[i][0];
//                            newArray[i][1] = list[i][1];
//                        }
//                        newArray[list.length][0] = txtFr.getText().toString();
//                        newArray[list.length][1] = txtAng.getText().toString();
//                        list = newArray;
                        list.add(new Model(txtAng.getText().toString(),txtFr.getText().toString()));
                    }else{
                        then = false;
                    }
                    
                // sinon c'est une modif
                } else {
					// modifier les mots du couple à la position du couple en cours de modification
                    EditText txt = findViewById(R.id.input_fr);
                    list.get(position).setFr(txt.getText().toString());
                    txt = findViewById(R.id.input_ang);
                    list.get(position).setAng(txt.getText().toString());
                }

                // si l'ajout a bien été effectuer (mots saisis correctes)
                if ( then ) {
                    Intent intent = new Intent(ModifActivity.this, ListActivity.class);

                    // sauvegarder les modifs
                    if ( RWFile.save(ModifActivity.this, list) ) {
                        // inform the user that the save is successful
                        Toast.makeText(ModifActivity.this,"Modifications effectuées",Toast.LENGTH_LONG).show();
                    }

                    // put a flag to specify the next activity like a new activity on this activity, the menu of the app
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    // start the new activity
                    startActivity(intent);
                }
            }
        });

		// listener du bouton "delete"
        but = findViewById(R.id.btn_delete);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				// si ce n'est pas un ajout, le bouton est alors actif
                if (position != -1) {
					// créer le tableau avec une place en moins, le copier sauf le couple à la position du couple en cours de modif
//                    String[][] newArray = new String[list.length - 1][2];
//                    int index = 0;
//                    for (int i = 0; i < list.length; i++) {
//                        if (i != position) {
//                            newArray[index][0] = list[i][0];
//                            newArray[index][1] = list[i][1];
//                            index++;
//                        }
//
//                    }
//                    list = newArray;
                    list.remove(position);

                    // sauvegarder les modifs
                    if ( RWFile.save(ModifActivity.this, list) ) {
                        // inform the user that the save is successful
                        Toast.makeText(ModifActivity.this,"Modifications effectuées",Toast.LENGTH_LONG).show();
                    }
                }

                Intent intent = new Intent(ModifActivity.this, ListActivity.class);

                // put a flag to specify the next activity like a new activity on this activity, the menu of the app
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                //start the new activity
                startActivity(intent);
            }
        });


    }


    @Override
    /**
     * Executée lorsque que le bouton de retour de la toolbar est appuyé.
     */
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
