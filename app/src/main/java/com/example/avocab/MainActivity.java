package com.example.avocab;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Activité de jeu où le choix de la bonne traduction est à faire (Page de jeu/quiz)
 **/
public class MainActivity extends AppCompatActivity {

    // la liste de mot
    private List<Model> list;

    // le n° de la case qui contient la bonne réponse. -2 si l'état de
    // l'appli est en mode affichage des réponses
    private int case_bonne_rep;

	// l'indice du couple de mots dans l'attribut "list" dont on cherche la traduction
    private int indice_data_rep;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialisation des attibuts
        this.case_bonne_rep = -1;
        this.indice_data_rep = 0;
        this.list = new ArrayList<>();

        // charger la liste de mots sauvegardés
        this.list = RWFile.load(this);

        // si aucun mots n'a été sauvegardés ou seulement moins de 4 (pour proposer des réponses) dans l'appli
        if ( this.list == null || this.list.size() < 4 ) {
            this.hideATH(true);
        }else{
            this.hideATH(false);

            // mettre tous les boutons en gris, et ajouter un listener
            Button but = findViewById(R.id.button_0);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
            this.setActionChoice(but,0);
            but = findViewById(R.id.button_1);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
            this.setActionChoice(but,1);
            but = findViewById(R.id.button_2);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
            this.setActionChoice(but,2);
            but = findViewById(R.id.button_3);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
            this.setActionChoice(but,3);

            // listener du bouton skip
            but = findViewById(R.id.button_skip);
            but.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    skipPress();
                }
            });

            // lancer le jeu
            this.nextQuestion();
        }

        // listener du bouton param
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ListActivity.class);
                startActivity(intent);
            }
        });
    }


	/**
	 * Listener pour les boutons de choix
     * @param but Le bouton à modifier
     * @param index La place du bouton sur l'écran (son indice)
	 **/
    public void setActionChoice(Button but, int index) {
        // TODO rendre le parametre final
        final int n = index;
        but.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
				// si les réponses sont affichées (-2 = la réponse est montré à l'écran après que l'user est tenté un coup)
                if ( case_bonne_rep != -2 ) {
                    choose(n);
                }
                // sinon les boutons sont inactifs
            }
        });
    }


	/**
	 * Méthode pour afficher un nouveau mot et montrer de nouvelles propositions
	 **/
    public void nextQuestion () {
        // Piste pour changer l'aléatoire
//        Date date = new Date();
//        System.out.println("- - - - - - - - - -"+(Integer.parseInt(((String)(date.getTime()+"")).substring(10))%4));

        // choix d'un mot aléatoire
        this.indice_data_rep = (int) Math.round(Math.random()*(this.list.size()-1));

        // effectuer une deuxième pioche aléatoire pour ajouter un peu plus de proba aux mots aux extrémités
        // de la liste (indice 0 et indice MAX) car java n'est pas équitablement aléatoire
        int edge = (int) Math.round(Math.random()*(this.list.size()-1));
        // proba = 4 chances sur (nb couple) que ce soit au final un des mots des extrémités
        if ( edge <= 3 ) {
            edge = (int) Math.round(Math.random());
            if ( edge <= 0.5 ) {
                this.indice_data_rep = 0;
            }else{
                this.indice_data_rep = this.list.size()-1;
            }
        }

        // mettre le mot francais dans le label de présentation
        TextView t = findViewById(R.id.quest_view);
        t.setText(this.list.get(this.indice_data_rep).getFr());

        // place aléatoire parmi les boutons pour mettre la bonne réponse
        this.case_bonne_rep = (int) Math.round(Math.random()*3);

        // mauvais mots aléatoires et différents pour les autres propositions
        int aleaA = this.indice_data_rep;
        while ( aleaA == this.indice_data_rep ) {
            aleaA = (int) Math.round(Math.random()*(this.list.size()-1));
        }
        int aleaB = this.indice_data_rep;
        // tant que l'indice du mot aléatoire n'est pas le même que la réponse,
        // ni de la première proposition
        while ( aleaB == this.indice_data_rep || aleaB == aleaA ) {
            aleaB = (int) Math.round(Math.random()*(this.list.size()-1));
        }
        int aleaC = this.indice_data_rep;
        while ( aleaC == this.indice_data_rep || aleaC == aleaA || aleaC == aleaB ) {
            aleaC = (int) Math.round(Math.random()*(this.list.size()-1));
        }

        Button button1 = findViewById(R.id.button_0);
        Button button2 = findViewById(R.id.button_1);
        Button button3 = findViewById(R.id.button_2);
        Button button4 = findViewById(R.id.button_3);

		// affichage des porpositions selon la position de la réponse
        if ( this.case_bonne_rep == 0 ) {
            button1.setText(this.list.get(this.indice_data_rep).getAng());
            button2.setText(this.list.get(aleaA).getAng());
            button3.setText(this.list.get(aleaB).getAng());
            button4.setText(this.list.get(aleaC).getAng());
        }else if ( this.case_bonne_rep == 1 ) {
            button1.setText(this.list.get(aleaA).getAng());
            button2.setText(this.list.get(this.indice_data_rep).getAng());
            button3.setText(this.list.get(aleaB).getAng());
            button4.setText(this.list.get(aleaC).getAng());
        }else if ( this.case_bonne_rep == 2 ) {
            button1.setText(this.list.get(aleaA).getAng());
            button2.setText(this.list.get(aleaB).getAng());
            button3.setText(this.list.get(this.indice_data_rep).getAng());
            button4.setText(this.list.get(aleaC).getAng());
        }else if ( this.case_bonne_rep == 3 ) {
            button1.setText(this.list.get(aleaA).getAng());
            button2.setText(this.list.get(aleaB).getAng());
            button3.setText(this.list.get(aleaC).getAng());
            button4.setText(this.list.get(this.indice_data_rep).getAng());
        }

    }


	/**
	 * Méthode qui se charge des changements à effectuer selon ce que le joueur à joué
	 * @param n La case jouée par l'utilisateur ( de 0 à 3 )
	 **/
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void choose (int n) {
		// mettre la bonne réponse en vert
        if ( this.case_bonne_rep == 0 ) {
            Button but = findViewById(R.id.button_0);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_good));
        }else if ( this.case_bonne_rep == 1 ) {
            Button but = findViewById(R.id.button_1);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_good));
        }else if ( this.case_bonne_rep == 2 ) {
            Button but = findViewById(R.id.button_2);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_good));
        }else if ( this.case_bonne_rep == 3 ) {
            Button but = findViewById(R.id.button_3);
            but.setBackground(getResources().getDrawable(R.drawable.choice_button_good));
        }
        
        // Si le joueur n'a pas la bonne réponse mettre les mauvaises réponses en rouge
        if ( n != this.case_bonne_rep ) {
            if ( this.case_bonne_rep == 0 ) {
                Button but = findViewById(R.id.button_1);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_2);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_3);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
            }else if ( this.case_bonne_rep == 1 ) {
                Button but = findViewById(R.id.button_0);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_2);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_3);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
            }else if ( this.case_bonne_rep == 2 ) {
                Button but = findViewById(R.id.button_0);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_1);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_3);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
            }else if ( this.case_bonne_rep == 3 ) {
                Button but = findViewById(R.id.button_0);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_1);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
                but = findViewById(R.id.button_2);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad));
            }else{
                System.out.println("what the fuck");
            }

            // mettre une couleur plus foncée pour le choix fait par l'utilisateur
            if ( n == 0 ) {
                Button but = findViewById(R.id.button_0);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad_choice));
            }else if ( n == 1 ) {
                Button but = findViewById(R.id.button_1);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad_choice));
            }else if ( n == 2 ) {
                Button but = findViewById(R.id.button_2);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad_choice));
            }else if ( n == 3 ) {
                Button but = findViewById(R.id.button_3);
                but.setBackground(getResources().getDrawable(R.drawable.choice_button_bad_choice));
            }
        }
        // passer l'activité en mode "réponses affichées"
        this.case_bonne_rep = -2;

        // afficher le bouton skip
        Button but = findViewById(R.id.button_skip);
        but.setVisibility(View.VISIBLE);
    }


	/**
	 * Méthode exécutée quand le bouton "Skip" est actionné. Lancement d'une nouvelle question, masquage
     * du bouton "skip", et retour au gris pour les boutons de propositions.
	 **/
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void skipPress () {
        // TODO fusionner skippress() avec nextQuestion() (du moins pour la couleur des bouton)
        // le bouton est pressé en tant que "Suivant" donc masquage des reponses
        Button but = findViewById(R.id.button_0);
        but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
        but = findViewById(R.id.button_1);
        but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
        but = findViewById(R.id.button_2);
        but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
        but = findViewById(R.id.button_3);
        but.setBackground(getResources().getDrawable(R.drawable.choice_button_idle));
        but = findViewById(R.id.button_skip);
        but.setVisibility(View.INVISIBLE);

        // état initial
        this.case_bonne_rep = -1;

        // mot suivant
        this.nextQuestion();
    }



    /**
     * Cache les boutons de proposition et le label de présentation si true est passé en paramètre.
     * Un label informant la nécéssité d'au moins 4 mots sauvegardés est alors affiché. Si false est
     * passé en paramètre, l'inverse se produit.
     * @param to_hide Si oui ou non il faut cacher les boutons et label pour le jeu
     */
    public void hideATH (boolean to_hide) {
        findViewById(R.id.button_0).setVisibility(to_hide ? View.INVISIBLE:View.VISIBLE);
        findViewById(R.id.button_1).setVisibility(to_hide ? View.INVISIBLE:View.VISIBLE);
        findViewById(R.id.button_2).setVisibility(to_hide ? View.INVISIBLE:View.VISIBLE);
        findViewById(R.id.button_3).setVisibility(to_hide ? View.INVISIBLE:View.VISIBLE);
        findViewById(R.id.quest_view).setVisibility(to_hide ? View.INVISIBLE:View.VISIBLE);

        findViewById(R.id.warning).setVisibility(to_hide ? View.VISIBLE:View.INVISIBLE);

    }


}
